﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MarkoTell.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string name { get; set; }
        public string surname { get; set; }
        [NotMapped]
        public string password { get; set; }
        [StringLength(1, ErrorMessage = "Previse znakova uneseno")]
        public string department { get; set; }
        [Range(1, 8)]
        public int grade { get; set; }
        [StringLength(50, ErrorMessage = "Previse znakova uneseno")]
        public string emailOfParents { get; set; }
        [StringLength(1, ErrorMessage = "Previse znakova uneseno")]
        public string gender { get; set; }
        [StringLength(200, ErrorMessage = "Previse znakova uneseno")]
        public string school { get; set; }
        [StringLength(200, ErrorMessage = "Previse znakova uneseno")]
        public string contestCountry { get; set; }
    }


}
