﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarkoTell.Models
{
    [Table("CF_Countrys")]
    public class Countries
    {

        [Key]
        public int CountryID { get; set; }


        [StringLength(255, ErrorMessage = "Previse znakova uneseno")]
        public string countryName { get; set; }
        public virtual ICollection<Schools> Schools { get; set; }

    }


    [Table("CF_Schools")]
    public class Schools
    {

        [Key]
        public int id { get; set; }
        public int CountryID { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public virtual Countries Countries { get; set; }


    }
}
