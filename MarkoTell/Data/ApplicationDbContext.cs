﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MarkoTell.Models;

namespace MarkoTell.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
   

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<Schools> Schools { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Schools>()
                .HasOne(f => f.Countries)
                .WithMany(s => s.Schools)
                .HasForeignKey(e => e.CountryID);

     
        }


    }
}
