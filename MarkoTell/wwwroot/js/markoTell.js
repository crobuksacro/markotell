﻿function populateModalFromAjax(url) {
    //$('#loader').show();
    $('.modal-content').html();

    $.ajax({
        url: url,
        success: function (response) { // on success..
            $('.modal-content').html(response);
            $('#markoTell_modal').modal('show');
            //$('#loader').hide();
        },
        error: function (response) {
            $('#markoTell_modal').modal('show');
            $('.modal-content').html("greška!");
            //$('#loader').hide();
        }

    });

}


function getSchools(element) {


    var id = $(element).find(":selected").val();

    $.ajax({
        url: '/GetSchools',
        contentType: "application/json; charset=utf-8",
        data: { id: id },
        success: function (response) { // on success..

            $('#schoolsOptions').html();
            var inputs = "";
            $(response.model).each(function () {

                inputs += '<option value=' + this.id + '>' + this.name + '</option>';
            });
            $('#schoolsOptions').html(inputs);


        },
        error: function (response) {
            alert("greška prilikom dohvačanja!");
        }

    });

}


function ElementsSelected(e) {
    e.preventDefault();
    if ($('#school').find(":selected").val() == "") {
        alert("molimo Vas da odaberete školu!");
    }
    else {


        var inputs = $("#RegistrationForm").find(".RegInput");
        var canIProceed = true;
        var fields = {};
        $(inputs).each(function () {
                fields[this.name] = $(this).val();
        });



        $.ajax({
            type: 'POST',
            url: '/MemberRegistration',
            data: JSON.stringify(fields),
            contentType: "application/json; charset=utf-8",
            success: function (response) { // on success..


            },
            error: function (response) {
                alert("greška!");
            }

        });




    }
    

}