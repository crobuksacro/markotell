﻿using System.Collections.Generic;
using System.Net;
using MarkoTell.Dal;
using MarkoTell.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace MarkoTell.Controllers
{
    public class MarkoTellController : Controller
    {

        HomePageRepo repo = new HomePageRepo();
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;

        public MarkoTellController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }

        [Route("ModalForRegistration")]
        public ActionResult ModalForRegistration()
        {
            return PartialView("/Views/Partials/RegistrationModal.cshtml", repo.getCountries().FindAll(b=>b.Schools.Count > 0));
        }


        [Route("GetSchools")]
        [HttpGet]
        public ActionResult getSchools(int id)
        {
            var schools = repo.getCountries().Find(b => b.CountryID == id).Schools;

            List<Schools> model = new List<Schools>();
            foreach (var li in schools)
            {
                Schools b = new Schools();
                b.Adress = li.Adress;
                b.id = li.id;
                b.Name = li.Name;
                model.Add(b);
            }
   

            return Json(new
            {
                model
            });
        }



        [Route("MemberRegistration")]
        [HttpPost]
        public ActionResult MemberRegistration(ApplicationUser user)
        {
            user.UserName = user.Email;
            var result = _userManager.CreateAsync(user, user.password).Result;
            if (result.Succeeded)
            {
                _signInManager.SignInAsync(user, isPersistent: true);
                _signInManager.PasswordSignInAsync(user, user.password,true,false);
                return RedirectToAction("Index", "Home");
            }
            return View("");
        }
    }
}