﻿using MarkoTell.Data;
using MarkoTell.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarkoTell.Dal
{
    public class HomePageRepo
    {


        public static DbContextOptions<ApplicationDbContext> options
        {
            get {
                var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
                optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=_MarkoTell;Integrated Security=true;MultipleActiveResultSets=true");
                return optionsBuilder.Options;
            }
        }


        public List<Countries> getCountries()
        {
            using (var db = new ApplicationDbContext(options))
            {
                return db.Countries.Include(b => b.Schools).ToList();
            }
        }


    }
}
