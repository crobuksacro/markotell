﻿using MarkoTell.Data;
using MarkoTell.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarkoTell.Dal
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            context.Database.EnsureCreated();
            if (!context.Countries.Any())
            {
                
                context.Countries.Add(new Countries() { countryName = "Hrvatska" });
                context.Countries.Add(new Countries() { countryName = "Bosna" });
                context.Countries.Add(new Countries() { countryName = "Rusija" });
                context.Countries.Add(new Countries() { countryName = "Mađarska" });
                context.SaveChanges();
            }
            if (!context.Schools.Any())
            {
                var getAllCountries = context.Countries.ToList();


                foreach (var li in getAllCountries)
                {
                    li.Schools = new List<Schools>();


                    for (int i=0; i<6; i++)
                    {
                        Schools sc = new Schools() { Adress = "Adress1", Name = "škola u "+ li.countryName+" br:"+i };
                        li.Schools.Add(sc);
                        context.SaveChanges();

                    }
                }
     
            }
        }
    }
}
